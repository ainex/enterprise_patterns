package task7;

public class ConcurrencyException extends RuntimeException {

	public ConcurrencyException() {
	}

	public ConcurrencyException(String message) {
		super(message);
	}
}
