package task7;


public class OptimisticOfflineLockExample {
	public static void main(String[] args) {
		long version = 123457;

		Thread threadOne = new Thread(new ModifyFileRunnable(version));
		threadOne.run();
		Thread threadTwo = new Thread(new ModifyFileRunnable(version, 1000));
		threadTwo.run();

	}
}
