package task7;

import java.io.File;
import java.io.IOException;

public class ModifyFileRunnable implements Runnable {
	long version;
	long delay;

	public ModifyFileRunnable(long version, long delay) {
		this.version = version;
		this.delay = delay;
	}

	public ModifyFileRunnable(long version) {
		this.version = version;
		delay = 0;
	}

	@Override
	public void run() {
		try {
			Thread.sleep(delay);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		File file = new File("file_ver_" + version + ".txt");

		//modify
		File fileRenamed = new File("file_ver_" + (version + 1) + ".txt");
		boolean renameTo = file.renameTo(fileRenamed);
		System.out.println("rename before update: " + renameTo);
		if(!renameTo){
			throw new  ConcurrencyException("file ver:"+ version +  " was modified");
		}
	}
}
