package task2;


import java.math.BigDecimal;
import java.sql.*;
import java.util.Date;


public class Gateway {

    private static final String DB_CONNECTION = "jdbc:postgresql://localhost:5432/enterprise_patterns";
    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "postgres";


    private Connection db;

    private static Connection getDBConnection() {
        Connection dbConnection = null;
        try {
            dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
            return dbConnection;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return dbConnection;
    }

    public Gateway() {
        db = getDBConnection();
    }

    public ResultSet findContract(int contractId) throws SQLException {
        PreparedStatement stmt = db.prepareStatement(findContractStatement);
        stmt.setInt(1, contractId);
        ResultSet result = stmt.executeQuery();
        return result;
    }

    public ResultSet findProduct(int productId) throws SQLException {
        PreparedStatement stmt = db.prepareStatement(findProductStatement);
        stmt.setInt(1, productId);

        ResultSet result = stmt.executeQuery();
        return result;
    }

    public void insertRecognition(int contract, BigDecimal amount, Date recognizedOn, int days) throws SQLException {

        PreparedStatement stmt = db.prepareStatement(insertRecognition);

        stmt.setInt(1, contract);
        stmt.setBigDecimal(2, amount);
        stmt.setDate(3, new java.sql.Date(recognizedOn.getTime()));
        stmt.setInt(4, days);
        stmt.executeUpdate();

    }

    private static final String insertRecognition = "INSERT INTO revenue_recognitions"
            + "(contract, amount, recognized_on, days) VALUES"
            + "(?,?,?,?)";

    private static final String findContractStatement =
            "SELECT * FROM contracts WHERE id = ?";

    private static final String findProductStatement =
            "SELECT * FROM products WHERE id = ?";
}
