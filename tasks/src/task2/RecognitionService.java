package task2;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;

public class RecognitionService {

    private Gateway gateway;

    public RecognitionService() {
        gateway = new Gateway();
    }

    public void recognizeRevenue(int contractId) {

        try {

            ResultSet rsContract = gateway.findContract(contractId);
            rsContract.next();
            BigDecimal revenue = rsContract.getBigDecimal("revenue");
            Date date = new Date(rsContract.getDate("date_signed").getTime());
            int productId = rsContract.getInt("product");

            ResultSet rsProduct = gateway.findProduct(productId);
            rsProduct.next();
            String productType = rsProduct.getString("type");
            BigDecimal oneThirdAllocation = revenue.divide(new BigDecimal(3), BigDecimal.ROUND_HALF_UP);

            if (productType.equals("S")) {

                gateway.insertRecognition(contractId, oneThirdAllocation, date, 0);
                gateway.insertRecognition(contractId, oneThirdAllocation, addDays(date, 60), 60);
                gateway.insertRecognition(contractId, oneThirdAllocation, addDays(date, 90), 90);

            } else if (productType.equals("W")) {
                gateway.insertRecognition(contractId, revenue, date, 0);
            } else if (productType.equals("D")) {
                gateway.insertRecognition(contractId, oneThirdAllocation, date, 0);
                gateway.insertRecognition(contractId, oneThirdAllocation, addDays(date, 30), 30);
                gateway.insertRecognition(contractId, oneThirdAllocation, addDays(date, 60), 60);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }



    private Date addDays(Date date, int days){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, days);
        return calendar.getTime();
    }
}
