package task4;


public class NotStableConnection implements Retryable {
    private static int attempts = 0;
    @Override
    public String connect() {
        if(attempts < 2 ){
            attempts++;
            throw new IllegalStateException("failed");
        } else return "Success";
    }
}
