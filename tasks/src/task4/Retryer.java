package task4;


public class Retryer implements Retryable {
    private Retryable retryable;

    public Retryer() {
        this.retryable = new NotStableConnection();
    }

    @Override
    public String connect() {

        while (true) {
            try {
                return retryable.connect();
            } catch (Exception e) {
                System.out.println("failed, one more try...");
            }
        }

    }
}
