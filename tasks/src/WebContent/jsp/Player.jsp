<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>


<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>PlayerFrontController Example</title>
</head>
<body>

<div align="left" style="margin: 50px;">
    <p>Add new player</p>
    <form action="/add-player">
        <p> Select player's type:
            <select name="type">
                <option value="F">Footballer</option>
                <option value="C">Cricketer</option>
            </select>
        </p>
        <label>Name:</label> <input type="text" name="name" size="20px"> <br>
        <p>Depends on player's type</p>
        <label>Club (F): </label><input type="text" name="club" size="20px"> <br><br>
        <label>Batting ave(C): </label><input type="number" name="ave" size="20px"> <br><br>
        <input type="submit" value="submit">
    </form>

</div>

</body>
</html>