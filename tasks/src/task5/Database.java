package task5;

import task3.Product;

public interface Database {
    Product select(String query);
}
