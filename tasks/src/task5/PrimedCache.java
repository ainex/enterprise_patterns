package task5;

import task3.Product;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PrimedCache implements Database {
    private static Map<Integer, Product> cache = new HashMap<>();
    private Database database = new ConcreteDatabase();

    public PrimedCache(List<String> expectedQueries) {
        for(String query: expectedQueries){
            cache.put(query.hashCode(), database.select(query));
        }
    }

    @Override
    public Product select(String query) {
        int hashCode = query.hashCode();
        if (cache.containsKey(hashCode)) {
            System.out.println("cache hit");
            return cache.get(hashCode);
        } else {
            System.out.println("cache miss");
            Product result = database.select(query);
            cache.put(hashCode, result);
            return result;
        }
    }


}
