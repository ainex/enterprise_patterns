package task5;


import task3.Product;

public class ConcreteDatabase implements Database {
    @Override
    public Product select(String query) {
        if (query.contains("1")) {
            return new Product(1, "product1", "type1");
        } else if (query.contains("2")) {
            return new Product(2, "product2", "type2");
        } else if (query.contains("3")) {
            return new Product(3, "product3", "type3");
        }
        return null;
    }
}
