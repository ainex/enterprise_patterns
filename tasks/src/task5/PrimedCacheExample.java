package task5;

import java.util.ArrayList;
import java.util.List;

public class PrimedCacheExample {
    public static void main(String[] args) {
        List<String> expectedQueries = new ArrayList<String>();
        expectedQueries.add("select * from product where id = 1");
        expectedQueries.add("select * from product where id = 2");
        Database database = new PrimedCache(expectedQueries);

        database.select(expectedQueries.get(0));
        database.select(expectedQueries.get(1));
        //cache miss
        database.select("select * from product where id = 3");

    }
}
