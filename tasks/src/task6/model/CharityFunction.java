package task6.model;


import java.util.List;

public class CharityFunction {
	private String name;
	private List<Player> players;

	public CharityFunction(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Player> getPlayers() {
		return players;
	}

	public void setPlayers(List<Player> players) {
		this.players = players;
	}
}
