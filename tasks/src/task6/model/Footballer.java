package task6.model;

public class Footballer extends Player {
	private String club;

	public Footballer(String name, String club) {
		super(name);
		this.club = club;
	}

	public Footballer(int id, String name, String club) {
		super(id, name);
		this.club = club;
	}

	public String getClub() {
		return club;
	}

	public void setClub(String club) {
		this.club = club;
	}
}
