package task6.model;

public class Cricketer extends Player{
	int battingAverage;

	public Cricketer(String name, int battingAverage) {
		super(name);
		this.battingAverage = battingAverage;
	}

	public Cricketer(int id, String name, int battingAverage) {
		super(id, name);
		this.battingAverage = battingAverage;
	}

	public int getBattingAverage() {
		return battingAverage;
	}

	public void setBattingAverage(int battingAverage) {
		this.battingAverage = battingAverage;
	}
}
