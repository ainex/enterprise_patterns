package task6.dao;

import java.sql.*;

public class CharityFunctionDataGateway {
	private static final String DB_CONNECTION = "jdbc:postgresql://localhost:5432/enterprise_patterns";
	private static final String DB_USER = "postgres";
	private static final String DB_PASSWORD = "postgres";

	private Connection getDBConnection() {
		Connection dbConnection = null;
		try {
			dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
			return dbConnection;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return dbConnection;
	}

	public CharityFunctionDataGateway() {

	}

	private static final String findAll =
			"SELECT * FROM charity_function JOIN charity_function_player on charity_function.id = charity_function_player.charity_id";

	private static final String findPlayers =
			"SELECT * FROM charity_function JOIN charity_function_player on charity_function.id = charity_function_player.charity_id " +
					"WHERE charity_function.id = ?";
}
