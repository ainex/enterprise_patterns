package task6.dao;

import java.sql.*;
import java.util.Map;

public class PlayerDataGateway {
	private static final String DB_CONNECTION = "jdbc:postgresql://localhost:5432/enterprise_patterns";
	private static final String DB_USER = "postgres";
	private static final String DB_PASSWORD = "postgres";

	private Connection getDBConnection() {
		Connection dbConnection = null;
		try {
			dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
			return dbConnection;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return dbConnection;
	}

	public PlayerDataGateway() {

	}

	public Integer insertCricketer(Map<String, Object> cricketer) {
		try (Connection connection = getDBConnection()) {
			int generatedId = insertPlayer(cricketer, connection);
			PreparedStatement stmt = connection.prepareStatement(insertCricketer);
			stmt.setInt(1, generatedId);
			stmt.setInt(2, (int) cricketer.get("battingAverage"));
			stmt.execute();
			stmt.close();
			return generatedId;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;

	}

	public Integer insertFootballer(Map<String, Object> footballer) {
		try (Connection connection = getDBConnection()) {
			int generatedId = insertPlayer(footballer, connection);
			PreparedStatement stmt = connection.prepareStatement(insertFootballer);
			stmt.setInt(1, generatedId);
			stmt.setString(2, (String) footballer.get("club"));
			stmt.execute();
			stmt.close();
			return generatedId;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public ResultSet findFootballerById(int id) {
		try (Connection connection = getDBConnection()) {
			PreparedStatement stmt = connection.prepareStatement(findById);
			stmt.setInt(1, id);
			return stmt.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	private static final String findById =
			"SELECT * FROM footballer JOIN person on person.id = footballer.id WHERE footballer.id = ?";

	private int insertPlayer(Map<String, Object> footballer, Connection connection) throws SQLException {
		PreparedStatement stmt = connection.prepareStatement(insertPlayer);
		stmt.setString(1, (String) footballer.get("name"));
		ResultSet rs = stmt.executeQuery();
		rs.next();
		int resultId = rs.getInt(1);
		stmt.close();
		rs.close();
		return resultId;
	}

	private static final String insertPlayer =
			"INSERT INTO player (name) VALUES (?) RETURNING id";

	private static final String insertCricketer =
			"INSERT INTO cricketer (id, batting_avarage) VALUES (?, ?)";

	private static final String insertFootballer =
			"INSERT INTO footballer (id, club) VALUES (?, ?)";
}
