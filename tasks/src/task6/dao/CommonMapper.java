package task6.dao;


import task6.model.CharityFunction;
import task6.model.Cricketer;
import task6.model.Footballer;

import java.sql.*;
import java.util.*;

public class CommonMapper {
	private PlayerDataGateway playerDataGateway = new PlayerDataGateway();
	private CharityFunctionDataGateway charityFunctionDataGateway = new CharityFunctionDataGateway();

	public Footballer findFootballerById(int id){
		ResultSet rsFootballers = null;
		try {
			rsFootballers = playerDataGateway.findFootballerById(id);
			if (null == rsFootballers) {
				return null;
			}
			if (rsFootballers.next()) {
				Footballer footballer = new Footballer(rsFootballers.getInt("id"), rsFootballers.getString("name"), rsFootballers.getString("club"));
				return footballer;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			closeRs(rsFootballers);
		}
		return null;
	}

	public Integer insertCricketer(Cricketer cricketer){
		Map<String, Object> fields = fromModel(cricketer);
		return  playerDataGateway.insertCricketer(fields);
	}

	public Integer insertFootballer(Footballer footballer){
		Map<String, Object> fields = fromModel(footballer);
		return playerDataGateway.insertFootballer(fields);
	}

	private Map<String, Object> fromModel(Cricketer cricketer) {
		Map<String, Object> fields = new HashMap<>();
		fields.put("name", cricketer.getName());
		fields.put("battingAverage", cricketer.getBattingAverage());
		return fields;
	}

	private Map<String, Object> fromModel(Footballer footballer) {
		Map<String, Object> fields = new HashMap<>();
		fields.put("name", footballer.getName());
		fields.put("club", footballer.getClub());
		return fields;
	}

	public List<CharityFunction> findAll(){
		List<CharityFunction> functions = new ArrayList<>();
		try {
			ResultSet charityFunctions = charityFunctionDataGateway.findAll();
			if (null == charityFunctions) {
				return null;
			}
			while (charityFunctions.next()) {
				CharityFunction function = new CharityFunction(charityFunctions.getString("name"));
				functions.add(function);
			}
			return functions;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;

	}

	private void closeRs(ResultSet resultSet) {
		if(null != resultSet){
			try {
				resultSet.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
