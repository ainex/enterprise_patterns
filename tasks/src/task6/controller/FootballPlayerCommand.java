package task6.controller;

import task6.model.Footballer;

public class FootballPlayerCommand extends FrontPlayerCommand {
	@Override
	public void process() {
		String name = request.getParameter("name");
		String club = request.getParameter("club");
		Footballer footballer = new Footballer(name, club);
		mapper.insertFootballer(footballer);
	}
}
