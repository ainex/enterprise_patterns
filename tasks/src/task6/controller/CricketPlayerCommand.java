package task6.controller;

import task6.model.Cricketer;

public class CricketPlayerCommand extends FrontPlayerCommand {
	@Override
	public void process() {
		String name = request.getParameter("name");
		int battingAverage = Integer.parseInt(request.getParameter("battingAverage"));
		Cricketer cricketer = new Cricketer(name, battingAverage);
		mapper.insertCricketer(cricketer);
	}
}
