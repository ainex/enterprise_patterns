package task6.controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class PlayerFrontController extends HttpServlet {

		public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
			FrontPlayerCommand command = getCommand(request);
			command.init(getServletContext(), request, response);
			command.process();
			response.sendRedirect("jsp/new-player.jsp");
		}

	private FrontPlayerCommand getCommand(HttpServletRequest request) {

			String type = request.getParameter("type");
			if ("F".equals(type)){
				return new FootballPlayerCommand();
			} else if ("C".equals(type)){
				return new CricketPlayerCommand();
			} else {
				throw new  RuntimeException("no such command");
			}
	}

}
