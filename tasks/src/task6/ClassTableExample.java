package task6;

import task6.dao.CommonMapper;
import task6.model.Cricketer;
import task6.model.Footballer;

import java.util.Random;

public class ClassTableExample {
	public static void main(String[] args) {

		CommonMapper mapper = new CommonMapper();
		Random random = new Random();
		int number = random.nextInt(1000);
		Cricketer cricketer = new Cricketer("cricketer" + number, number);
		mapper.insertCricketer(cricketer);
		Footballer footballer = new Footballer("footballer" + number, "club" + number);
		mapper.insertFootballer(footballer);
	}
}
