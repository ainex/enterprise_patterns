CREATE TABLE player (
  id   SERIAL PRIMARY KEY,
  name VARCHAR
);

CREATE TABLE footballer (
  id   INT PRIMARY KEY,
  club VARCHAR
);

CREATE TABLE cricketer (
  id              INT PRIMARY KEY,
  batting_avarage INT
);

CREATE TABLE charity_function (
  id   SERIAL PRIMARY KEY,
  name VARCHAR
);

CREATE TABLE charity_function_player (
  id         SERIAL,
  charity_id INT,
  player_id  INT,
  FOREIGN KEY (player_id) REFERENCES player (id),
  FOREIGN KEY (charity_id) REFERENCES charity_function (id)
);
-- INSERT INTO contracts (ID, product, revenue, date_signed) VALUES (1,1,15000, now());
