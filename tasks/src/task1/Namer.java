package task1;

public abstract class Namer {
    protected String fName;
    protected String lName;

    @Override
    public String toString() {
        return "task1.Namer{" +
                "fName='" + fName + '\'' +
                ", lName='" + lName + '\'' +
                '}';
    }
}

