package task1;

public class Main {

    public static void main(String[] args) {
        NameFactory factory = new NameFactory();
        Namer namer = factory.getName("task1.FirstFirst");
        System.out.println(namer);
    }
}
