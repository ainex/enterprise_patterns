package task1;

public class NameFactory {
    public NameFactory() {
    }

    Namer getName(String name) {
        if ("task1.FirstFirst".equals(name)) {
            return new FirstFirst(name);
        } else if ("task1.LastFirst".equals(name)) {
            return new LastFirst(name);
        } else return null;
    }
}
