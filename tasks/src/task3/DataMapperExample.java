package task3;

import java.util.List;

public class DataMapperExample {

    public static void main(String[] args) {

        ProductMapper mapper = new ProductMapper();
        List<Product> products = mapper.findAll();
        System.out.println(products);
    }
}
