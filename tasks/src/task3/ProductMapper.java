package task3;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProductMapper {
    private TableDataGateway tableDataGateway;

    public ProductMapper() {
        tableDataGateway = new TableDataGateway();
    }

    public List<Product> findAll() {
        List<Product> result = new ArrayList<>();
        try {
            ResultSet rsProducts = tableDataGateway.findAllProducts();
            if (null == rsProducts) {
                return result;
            }
            while (rsProducts.next()) {
                Product product = new Product(rsProducts.getInt("id"), rsProducts.getString("name"), rsProducts.getString("type"));
                result.add(product);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

}
