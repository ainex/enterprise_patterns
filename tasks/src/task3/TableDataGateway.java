package task3;

import java.sql.*;

public class TableDataGateway {

    private static final String DB_CONNECTION = "jdbc:postgresql://localhost:5432/enterprise_patterns";
    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "postgres";

    private static Connection getDBConnection() {
        Connection dbConnection = null;
        try {
            dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
            return dbConnection;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return dbConnection;
    }

    public TableDataGateway() {

    }

    ResultSet findAllProducts() {
        try (Connection connection = getDBConnection()) {
            PreparedStatement stmt = connection.prepareStatement(findAllProductsStatement);
            return stmt.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static final String findAllProductsStatement =
            "SELECT * FROM products";
}
