package homework1;

import homework1.controller.AppointmentController;
import homework1.dao.AppointmentTableModule;
import homework1.dao.impl.AppointmentTableModuleImpl;
import homework1.view.AppointmentView;

public class BeautySalonApplication {
	public static void main(String[] args) {
//		AppointmentTableModule tableModule = new AppointmentTableModuleImpl();
//		AppointmentFilter filter = new AppointmentFilter();
//		filter.setDateFrom(LocalDateTime.of(2017, 1, 1, 0, 0));
//		filter.setDateTo(LocalDateTime.of(2017,4,18,0,0));
//
//		List<AppointmentDTO> appointmentDTOList = tableModule.findAppointments(filter);
//		System.out.println(appointmentDTOList);

		AppointmentView appointmentView = new AppointmentView();

		AppointmentTableModule appointmentTableModule = new AppointmentTableModuleImpl();

		AppointmentController appointmentController = new AppointmentController(appointmentView, appointmentTableModule);

		appointmentView.setVisible(true);

	}
}

