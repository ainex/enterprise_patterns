package homework1;

import java.time.LocalDateTime;

public class AppointmentFilter {
	private LocalDateTime dateFrom;
	private LocalDateTime dateTo;

	public LocalDateTime getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(LocalDateTime dateFrom) {
		this.dateFrom = dateFrom;
	}

	public LocalDateTime getDateTo() {
		return dateTo;
	}

	public void setDateTo(LocalDateTime dateTo) {
		this.dateTo = dateTo;
	}
}
