CREATE TABLE appointment (
  id          INT PRIMARY KEY,
  date        TIMESTAMP NOT NULL,
  client_id   INT       NOT NULL,
  employee_service_id INT NOT NULL,
  FOREIGN KEY (employee_service_id) REFERENCES employee_service_lnk (id)
);

CREATE TABLE service (
  id              INT PRIMARY KEY,
  service_type_id INT     NOT NULL,
  price           DECIMAL NOT NULL,
  duration        TIME
);

CREATE TABLE service_type (
  id         INT PRIMARY KEY,
  name       VARCHAR NOT NULL,
  descripion VARCHAR
);

CREATE TABLE person (
  id        INT PRIMARY KEY,
  full_name VARCHAR(200),
  phone     VARCHAR(50)
);

CREATE TABLE employee (
  id         INT PRIMARY KEY,
  job_title  VARCHAR,
  start_date DATE,
  FOREIGN KEY (id) REFERENCES person (id)
);

CREATE TABLE client (
  id                INT PRIMARY KEY,
  bonus_card_number INT,
  first_visit_date  DATE,
  FOREIGN KEY (id) REFERENCES person (id)
);

CREATE TABLE employee_service_lnk (
  id          INT PRIMARY KEY,
  employee_id INT NOT NULL,
  service_id  INT NOT NULL,
  FOREIGN KEY (employee_id) REFERENCES employee (id),
  FOREIGN KEY (service_id) REFERENCES service (id)
);

INSERT INTO person (id, full_name, phone) VALUES (1, 'Andrew Cole', '48-(153)805-5905');
INSERT INTO person (id, full_name, phone) VALUES (2, 'Robin Wright', '1-(605)553-4789');
INSERT INTO person (id, full_name, phone) VALUES (3, 'Joan Jordan', '7-(922)841-5062');
INSERT INTO person (id, full_name, phone) VALUES (4, 'Michael Cook', '86-(248)440-4597');

INSERT INTO employee (id, job_title, start_date) VALUES (1, 'Manicurist', '2016-06-14');
INSERT INTO employee (id, job_title, start_date) VALUES (2, 'Hairdresser', '2014-12-01');

INSERT INTO client (id, bonus_card_number, first_visit_date) VALUES (3, 123456, '2016-04-19');
INSERT INTO client (id, bonus_card_number, first_visit_date) VALUES (4, 456123, '2017-03-01');

INSERT INTO service_type (id, name, descripion) VALUES (1, 'Manicure', NULL);
INSERT INTO service_type (id, name, descripion) VALUES (2, 'Haircut', NULL);


INSERT INTO service (id, service_type_id, price, duration) VALUES (1, 1, 15.00, '00:45:00');
INSERT INTO service (id, service_type_id, price, duration) VALUES (2, 2, 30.00, '01:00:00');

INSERT INTO employee_service_lnk (id, employee_id, service_id) VALUES (1, 1, 1);
INSERT INTO employee_service_lnk (id, employee_id, service_id) VALUES (2, 2, 2);


INSERT INTO appointment (id, date, client_id,employee_service_id) VALUES (1,'2017-04-17 10:00:00',3,1);
INSERT INTO appointment (id, date, client_id,employee_service_id) VALUES (2,'2017-04-17 11:00:00',3,2);
INSERT INTO appointment (id, date, client_id,employee_service_id) VALUES (3,'2017-04-17 11:00:00',4,1);




