package homework1.dao;


import homework1.AppointmentDTO;
import homework1.AppointmentFilter;

import java.util.List;

public interface AppointmentTableModule {

	List<AppointmentDTO> findAppointments(AppointmentFilter filter);
}
