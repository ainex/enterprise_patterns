package homework1.dao.impl;

import homework1.*;
import homework1.dao.AppointmentMapper;
import homework1.dao.AppointmentTableModule;

import java.util.List;

public class AppointmentTableModuleImpl implements AppointmentTableModule {

	private AppointmentTableModule mapper = new AppointmentMapper();
	@Override
	public List<AppointmentDTO> findAppointments(AppointmentFilter filter)  {
		List<AppointmentDTO> result =  mapper.findAppointments(filter);
		return result;
	}
}
