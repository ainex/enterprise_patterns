package homework1.dao;


import homework1.AppointmentFilter;
import homework1.exception.ApplicationException;

import java.sql.*;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class AppointmentGateway {

	private static final String DB_CONNECTION = "jdbc:postgresql://localhost:5432/enterprise_patterns";
	private static final String DB_USER = "postgres";
	private static final String DB_PASSWORD = "postgres";


	private Connection getDBConnection() {
		try {
			Connection dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
			return dbConnection;
		} catch (SQLException e) {
			throw new ApplicationException("driver manager error", e);
		}
	}


	public List<Map<String, Object>> findAppointments(AppointmentFilter filter) {
		PreparedStatement stmt = null;
		ResultSet resultSet = null;
		try (Connection db = getDBConnection()) {
			List<Object> params = new ArrayList<>();
			String query = buildQuery(filter, params);

			stmt = db.prepareStatement(query);
			setParameters(params, stmt);
			resultSet = stmt.executeQuery();

			return parseResults(resultSet);
		} catch (SQLException e) {
			throw new ApplicationException("database error", e);
		} finally {
			closeStatement(stmt, resultSet);
		}

	}

	private List<Map<String, Object>> parseResults(ResultSet resultSet) throws SQLException {
		ResultSetMetaData meta = resultSet.getMetaData();
		Integer columnCount = meta.getColumnCount();
		List<String> columnNames = new ArrayList<>();
		int count = 1 ; // start counting from 1 always
		while(count<=columnCount){
			columnNames.add(meta.getColumnLabel(count));
			count++;
		}

		List<Map<String, Object>> appointments = new ArrayList<>();
		while (resultSet.next()) {
			Map<String, Object> row = new HashMap<>();
			row.put("appId", resultSet.getInt("appId"));
			row.put("appDate", resultSet.getObject("appDate", LocalDateTime.class));
			row.put("clientFullName", resultSet.getString("clientFullName"));
			row.put("employeeFullName", resultSet.getString("employeeFullName"));
			row.put("duration", resultSet.getObject("duration", LocalTime.class));
			row.put("serviceName", resultSet.getString("serviceName"));
			appointments.add(row);
		}
		return appointments;
	}

	private void closeStatement(PreparedStatement stmt, ResultSet result) {
		if (null != stmt) {
			try {
				stmt.close();
			} catch (SQLException e) {
				throw new ApplicationException("database error", e);
			}
		}
		if (null != result) {
			try {
				result.close();
			} catch (SQLException e) {
				throw new ApplicationException("database error", e);
			}
		}
	}

	private void setParameters(List<Object> params, PreparedStatement stmt) throws SQLException {
		for (Object object : params) {
			stmt.setObject(params.indexOf(object) + 1, object);
		}
	}

	private String buildQuery(AppointmentFilter filter, List<Object> params) {
		String query = FIND_APPOINTMENTS;
		if (notEmpty(filter)) {
			query = query + WHERE;
		}
		boolean hasWhereClause = false;

		if (filter.getDateFrom() != null) {

			if (hasWhereClause) {
				query = query + AND + APPOINTMENT_DATE_GE;
			} else {
				hasWhereClause = true;
				query = query + APPOINTMENT_DATE_GE;
			}
			params.add(filter.getDateFrom());
		}

		if (filter.getDateTo() != null) {

			if (hasWhereClause) {
				query = query + AND + APPOINTMENT_DATE_LE;
			} else {
				hasWhereClause = true;
				query = query + APPOINTMENT_DATE_LE;
			}
			params.add(filter.getDateTo());
		}
		return query + ORDER_APPOINTMENT_DATE;
	}

	private boolean notEmpty(AppointmentFilter filter) {
		return filter.getDateFrom() != null || filter.getDateTo() != null;
	}


	private static final String FIND_APPOINTMENTS =
			"SELECT app.id appId, app.date appDate, " +
					"personClient.full_name clientFullName, " +
					"personEmployee.full_name employeeFullName, " +
					"service.duration duration, type.name serviceName " +
					"FROM appointment app " +
					"JOIN employee_service_lnk  skill on app.employee_service_id = skill.id " +
					"JOIN person personClient on app.client_id = personClient.id " +
					"JOIN employee employee ON skill.employee_id = employee.id " +
					"JOIN person personEmployee on employee.id = personEmployee.id " +
					"JOIN service service on skill.service_id = service.id " +
					"JOIN service_type type on type.id = service.service_type_id";

	private static final String WHERE = " WHERE ";
	private static final String AND = " AND ";
	private static final String APPOINTMENT_DATE_GE = " app.date >= ? ";
	private static final String APPOINTMENT_DATE_LE = " app.date <= ?";
	private static final String ORDER_APPOINTMENT_DATE = " order by app.date";

}
