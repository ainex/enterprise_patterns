package homework1.dao;


import homework1.AppointmentDTO;
import homework1.AppointmentFilter;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class AppointmentMapper implements AppointmentTableModule {
	private AppointmentGateway appointmentGateway = new AppointmentGateway();

	@Override
	public List<AppointmentDTO> findAppointments(AppointmentFilter filter) {
		List<Map<String, Object>> results = appointmentGateway.findAppointments(filter);
		List<AppointmentDTO> appointments = results.stream()
				.map(mapToDTO()).collect(Collectors.toList());

		return appointments;
	}

	private Function<Map<String, Object>, AppointmentDTO> mapToDTO() {
		return r -> {
			AppointmentDTO appointment = new AppointmentDTO();
			appointment.setId((int) r.get("appId"));
			appointment.setDate((LocalDateTime) r.get("appDate"));
			appointment.setClientFullName((String) r.get("clientFullName"));
			appointment.setEmployeeFullName((String) r.get("employeeFullName"));
			appointment.setDuration((LocalTime) r.get("duration"));
			appointment.setServiceName((String) r.get("serviceName"));
			return appointment;
		};
	}
}
