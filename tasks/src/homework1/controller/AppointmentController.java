package homework1.controller;

import homework1.AppointmentDTO;
import homework1.AppointmentFilter;
import homework1.dao.AppointmentTableModule;
import homework1.view.AppointmentView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDateTime;
import java.util.List;


public class AppointmentController {

	private AppointmentView appointmentView;
	private AppointmentTableModule appointmentTableModule;

	public AppointmentController(AppointmentView appointmentView, AppointmentTableModule appointmentTableModule) {
		this.appointmentView = appointmentView;
		this.appointmentTableModule = appointmentTableModule;

		this.appointmentView.addFindListener(new FindListener());
	}

	class FindListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {

			LocalDateTime dateFrom, dateTo;

			try{

				AppointmentFilter filter = new AppointmentFilter();
				dateFrom = appointmentView.getDateFrom();
				dateTo = appointmentView.getDateTo();
				filter.setDateFrom(dateFrom);
				filter.setDateTo(dateTo);

				List<AppointmentDTO> appointments = appointmentTableModule.findAppointments(filter);

				appointmentView.setAppointments(appointments);

			}

			catch(NumberFormatException ex){

				System.out.println(ex);

				appointmentView.displayErrorMessage("Please input date in format '2017-04-10'");

			}

		}

	}

}