package homework1.view;


import homework1.AppointmentDTO;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public class AppointmentView extends JFrame {

	private JLabel labelDateFrom = new JLabel("Date from:");
	private JTextField dateFrom = new JTextField(8);
	private JTextField timeFrom = new JTextField(5);
	private JLabel labelDateTo = new JLabel("Date to:");
	private JTextField dateTo = new JTextField(8);
	private JTextField timeTo = new JTextField(5);
	private JButton searchButton = new JButton("Search");

	private JTextArea searchResult = new JTextArea(10, 60);


	public AppointmentView() {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(700, 300);
		this.setTitle("Beauty Salon: Appointments");

		JPanel searchPanel = new JPanel();
		searchPanel.setLayout(new FlowLayout());
		searchPanel.add(labelDateFrom);
		dateFrom.setText(LocalDate.now().minusDays(6).toString());
		timeFrom.setText("10:00");
		searchPanel.add(dateFrom);
		searchPanel.add(timeFrom);
		searchPanel.add(labelDateTo);
		dateTo.setText(LocalDate.now().toString());
		timeTo.setText("20:00");
		searchPanel.add(dateTo);
		searchPanel.add(timeTo);

		searchPanel.add(searchButton);
		searchPanel.add(searchResult);

		JPanel appointmentsPanel = new JPanel();

		this.add(searchPanel);

	}

	public LocalDateTime getDateFrom() {
		return getLocalDateTime(dateFrom, timeFrom);
	}

	private LocalDateTime getLocalDateTime(JTextField dateField, JTextField timeField ) {
		String dateText = dateField.getText();
		if("".equals(dateText)) return null;
		String timeText = timeField.getText();
		if("".equals(timeText)) {
			timeText = "00:00";
		}
		return LocalDateTime.parse(dateText + "T" + timeText);
	}

	public LocalDateTime getDateTo() {
		return getLocalDateTime(dateTo, timeTo);
	}


	public void setAppointments(List<AppointmentDTO> appointments) {
		searchResult.setText("");

		for(AppointmentDTO appointment: appointments){
			searchResult.append(appointment.toStringPretty());
			searchResult.append("\n");
		}
	}

	public void addFindListener(ActionListener listenForSearchButton) {
		searchButton.addActionListener(listenForSearchButton);
	}


	public void displayErrorMessage(String errorMessage) {
		JOptionPane.showMessageDialog(this, errorMessage);
	}

}
