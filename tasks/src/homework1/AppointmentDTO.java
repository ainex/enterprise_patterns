package homework1;


import java.time.LocalDateTime;
import java.time.LocalTime;

public class AppointmentDTO {
	private int id;
	private LocalDateTime date;
	private String clientFullName;
	private String employeeFullName;
	private String serviceName;
	private LocalTime duration;

	public AppointmentDTO() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public String getClientFullName() {
		return clientFullName;
	}

	public void setClientFullName(String clientFullName) {
		this.clientFullName = clientFullName;
	}

	public String getEmployeeFullName() {
		return employeeFullName;
	}

	public void setEmployeeFullName(String employeeFullName) {
		this.employeeFullName = employeeFullName;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public LocalTime getDuration() {
		return duration;
	}

	public void setDuration(LocalTime duration) {
		this.duration = duration;
	}

	@Override
	public String toString() {
		return "AppointmentDTO{" +
				"id=" + id +
				", date=" + date +
				", clientFullName='" + clientFullName + '\'' +
				", employeeFullName='" + employeeFullName + '\'' +
				", serviceName='" + serviceName + '\'' +
				", duration=" + duration +
				'}';
	}
	public String toStringPretty(){
		return 	"date=" + date +
				", client='" + clientFullName + '\'' +
				", employee='" + employeeFullName + '\'' +
				", service='" + serviceName + '\'' +
				", duration=" + duration ;
	}
}
